from __future__ import absolute_import
from allot_worker.tasks import (
    longtime_sql_query,
    connection,
)
import sqlite3


sql_create_table = """
CREATE TABLE EMP (
        EmployeeID PRIMARY KEY,
        EmployeeName VARCRCHAR(100) UNIQUE NOT NULL,
        Salary DECIMAL(15, 2));
"""
sql_insert = """
INSERT INTO EMP (EmployeeID, EmployeeName, Salary)
VALUES (1, "Ian Ivanov", 40000.00),
(2, "Jack Nikson", 50000.00)
"""
sql_select = """
SELECT * FROM EMP;
"""


if __name__ == '__main__':
    # create db with table
    try:
        connection.execute(sql_create_table)
        connection.execute(sql_insert)
        connection.commit()
        output = connection.execute(sql_select).fetchall()
        [print(row) for row in output]
    except sqlite3.OperationalError as e:
        print('sqlite error:', e.args[0])  # table companies already exists
        output = connection.execute(sql_select).fetchall()
        [print(row) for row in output]
    with open('queries.txt', 'r') as ff:
        while True:
            query = ff.readline()
            if not query:
                break
            print(f'Query: {query}')
            result = longtime_sql_query.delay(query)
            print(f'Celery Task result: {result.result}')
