from __future__ import absolute_import
import logging
from celery import Celery
import sqlite3


logging.basicConfig(filename='allot_worker.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('allot_worker')
logging.warning('This will get logged to a file')

celery_app = Celery('allot_worker',
                    # broker='amqp://admin:mypass@rabbit:5672',
                    broker='amqp://admin:mypass@localhost:5672',
                    # broker='amqp://guest@localhost:5672',
                    backend='rpc://',
                    include=['allot_worker.tasks']
                    )
celery_app.conf.CELERY_ALWAYS_EAGER = True
celery_app.conf.CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

connection = sqlite3.connect('example.db')


@celery_app.task(bind=True, default_retry_delay=10)  # set a retry delay, 10 equal to 10s
def longtime_sql_query(self, query):
    print('sql longtime task stars')
    output = ''
    try:
        output = connection.execute(query).fetchall()
    except sqlite3.OperationalError as e:
        print('sqlite error:', e.args[0])
    return output

