import pytest
import json
import requests
from project.etl_server import (
    app as flask_app,
    setup_db
)


FILE_NAMES = [
    'file1.txt',
    'file2.txt',
    'file3.txt',
    'file4.txt',
    'file5.txt',
    'file6.corrupted.txt',
    'file7.txt'
]


@pytest.fixture
def client():
    app = flask_app.test_client()
    yield app


def test_base_route(client):

    setup_db()
    response = client.get('/')
    assert response.get_data() == b'Hello, World!'
    assert response.status_code == 200


def test_upload(client):
    data = json.dumps(FILE_NAMES)
    response = client.post("/upload", json=data)
    assert response.status_code == 200
