FROM python:3.6
COPY .requirements.txt /app/requirements.txt
COPY .allot_worker /app/
WORKDIR /app/
RUN pip3 install -r requirements.txt
CMD celery -A allot_worker.tasks worker --loglevel=info
