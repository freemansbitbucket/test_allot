import json
import sqlite3
from flask import (
    Flask,
    request,
    jsonify
)


app = Flask(__name__)
connection = sqlite3.connect('etl.db')


CREATE_TABLE_QUERY = """
CREATE TABLE files (
        file_name VARCRCHAR(100) UNIQUE NOT NULL,
        date DATETIME DEFAULT CURRENT_TIMESTAMP
        );
"""

# CREATE_TABLE_QUERY = """
# CREATE TABLE files (
#         file_name VARCRCHAR(100) UNIQUE NOT NULL
#         );
# """



APPEND_QUERY_TEMPLATE = """
INSERT INTO files (file_name)
VALUES ('{0}');
"""

CHECK_QUERY = """
SELECT * FROM files;
"""


@app.route('/')
def hello_world():
    return 'Hello, World!'


def add_filename_to_table(filename):
    try:
        connection.execute(APPEND_QUERY_TEMPLATE.format(filename))
        connection.commit()
    except sqlite3.DatabaseError as e:
        print('sqlite error:', e)


@app.route('/upload', methods=['POST'])
def hello():
    if not request.is_json:
        return 'not json', 400
    output = ''
    filename_list = json.loads(request.get_json())
    if not isinstance(filename_list, list):
        return 'not list', 400
    for filename in filename_list:
        if not 'corrupted' in filename:
            add_filename_to_table(filename)
    try:
        output = connection.execute(CHECK_QUERY).fetchall()
        print([row for row in output])
    except sqlite3.DatabaseError as e:
        print('sqlite error:', e)
    return jsonify(output), 200


def setup_db():
    try:
        connection.execute(CREATE_TABLE_QUERY)
        connection.commit()
    except sqlite3.OperationalError as e:
        print('sqlite error:', e)  # table companies already exists


if __name__ == '__main__':
    setup_db()
    app.run(port=8081, processes=4)
