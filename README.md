# test_allot
# to get platform running do:
sudo docker-compose up --build

# SQL to get second highest salary
#simple way:
SELECT EmployeeName, MIN(Salary) 
FROM
    (SELECT DISTINCT *, Salary 
        FROM EMP
        ORDER BY Salary DESC LIMIT 2
    );
# way for any db engine
SELECT EmployeeName, MAX(Salary)
FROM EMP
WHERE Salary NOT IN
    (SELECT DISTINCT MAX(Salary)
     FROM EMP);